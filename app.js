'use strict';

const readLine = require( 'readline' );
const jsonUtil = require( 'zayni/json' );
const logger   = require( 'zayni/log' );
const Client   = require( 'zayni/serviceHost' ).ServiceClient;

logger.useConfig( {
    enableConsoleLog : true,
    enableFileLog    : true,
    fileLogPath      : __dirname + '/ClientLog/zayni.logger.test.log',
    logFileMaxSize   : 2
} );

let client = new Client();
client.connect( '127.0.0.1', 7765 );

let rl = readLine.createInterface( {
    input  : process.stdin,
    output : process.stdout	
} );

rl.on( 'line', function ( command ) {
    if ( 'send' === command ) {
        let data = {
            userId   : 'pony001',
            userName : 'Pony Lin',
            sex      : 1,
            age      : 31
        };

        client
            .rpc( 'Test', data )
            .then( responsePackage => {
                let json = jsonUtil.serialize( responsePackage );
                console.log( 'RPC response:' );
                console.log( `requestId: ${responsePackage.requestId}` );
                console.log( `isSuccess: ${responsePackage.isSuccess}` );
                console.log( json );
            } )
            .catch( error => {
                console.error( `RPC error: ${error}` );
            } );
    }
} );


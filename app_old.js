const readLine = require( 'readline' );
const net      = require( 'net' );
const Socket   = net.Socket;

const socket = new Socket( { readable : true, writable : true } );

socket.connect( 889, '127.0.0.1', function ( error ) {
    console.log( 'Connect success.' );
} );

socket.on( 'data', function ( data ) {
    console.log( data + '' );
} );

let request = {
    requestId  : 'sss333',
    actionName : 'Test',
    data       : {
        userId   : 'pony001',
        userName : 'Pony Lin',
        sex      : 1,
        age      : 31
    },
    requestTime : new Date()
};

let rl = readLine.createInterface( {
    input  : process.stdin,
    output : process.stdout	
} );

rl.on( 'line', function ( command ) {
    if ( 'send' === command ) {
        let json = JSON.stringify( request ) + '\n';
        socket.write( new Buffer( json ) );
    }
} );
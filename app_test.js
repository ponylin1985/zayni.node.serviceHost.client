'use strict';

const readLine = require( 'readline' );
const client = require( './client_test.js' );
const logger = require( 'zayni/log' );

logger.useConfig( {
    enableConsoleLog : true,
    enableFileLog    : true,
    fileLogPath      : __dirname + '/ServerLog/zayni.logger.test.log',
    logFileMaxSize   : 2
} );

client.connect();

let rl = readLine.createInterface( {
    input  : process.stdin,
    output : process.stdout	
} );

rl.on( 'line', function ( command ) {
    if ( 'send' === command ) {
        let data = {
            userId   : 'pony001',
            userName : 'Pony Lin',
            sex      : 1,
            age      : 31
        };

        client.rpc( data )
            .then( responsePackage => {
                console.log( 'RPC response:' );
                console.log( JSON.stringify( responsePackage ) );
            } )
            .catch( error => {
                console.error( `RPC error: ${error}` );
            } );
    }
} );


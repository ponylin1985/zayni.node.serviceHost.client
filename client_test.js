'use strict';

const net      = require( 'net' );
const common   = require( 'zayni' ).common;
const guid     = require( 'zayni' ).guid;
const ext      = require( 'zayni/extensions' );
const jsonUtil = require( 'zayni/json' );
const Socket   = net.Socket;
const socket   = new Socket( { readable : true, writable : true } );

const requests = [];

socket.on( 'data', function ( data ) {
    console.log( data + '' );
    let json = ( data + '' ).trim();

    let responsePackage = jsonUtil.deserialize( json );

    setImmediate( () => {
        let reqId    = responsePackage.requestId;
        let req      = requests.where( r => r.requestId === reqId ).firstOrDefault();
        req.response = responsePackage;
    } );
} );

function send( data ) {
    return new Promise( ( resolve, reject ) => {
        let request = {
            requestId  : guid.createGuid(),
            actionName : 'Test',
            data       : data,
            requestTime : new Date()
        };

        let json = JSON.stringify( request ) + '\n';
        socket.write( new Buffer( json ) );
        requests.push( request );

        setInterval( () => {
            let r = requests.where( q => q.requestId === request.requestId );

            if ( common.isNullOrEmpty( r ) || 0 === r.length ) {
                return;
            }

            let req = r.firstOrDefault();

            if ( common.isNullOrEmpty( req ) ) {
                return;
            }

            let responsePackage = req.response;

            if ( common.isNullOrEmpty( req ) ) {
                return reject( new Error( 'No ResponsePackage obj.' ) );
            }

            responsePackage = jsonUtil.serialize( responsePackage );
            return resolve( responsePackage );
        }, 5 );
    } );
}

const client = {

    connect : function () {
        socket.connect( 889, '127.0.0.1', function ( error ) {
            console.log( 'Connect success.' );
        } );
    },

    rpc : function ( data ) {
        return send( data );
    },

    rcp_222 : function ( data ) {
        return send
            .then( reqPackage => {
                reqPackage.isTimeout = false;
                requests.set( reqPackage.requestId, reqPackage );


            } );
    }
};

module.exports = client;